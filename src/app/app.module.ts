import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { HttpModule ,Http } from  '@angular/http';

import { AppComponent } from './app.component';
import { PrinciComponent } from './princi/princi.component';
import { ConnComponent } from './conn/conn.component';
import { SignupComponent } from './signup/signup.component';
import { SigninComponent } from './signin/signin.component';
import { ForgotComponent } from './forgot/forgot.component';


@NgModule({
  declarations: [
    AppComponent,
    PrinciComponent,
    ConnComponent,
    SignupComponent,
    SigninComponent,
    ForgotComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot([
      {
        path: '',
        component: PrinciComponent
      },
      {
        path: 'login',
        component: ConnComponent
      },
      {
        path: 'signin',
        component: SigninComponent
      }
      ,
      {
        path: 'signup',
        component: SignupComponent
      },
      {
        path: 'forgot',
        component: ForgotComponent
      }
  ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
